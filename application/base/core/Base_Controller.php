<?php
require_once( BASEPATH . 'core/Controller.php' );

class Base_Controller extends CI_Controller 
{
    private $class      = '';
    private $uriclass   = '';
    private $template   = '';
    private $layout     = '';
    private $layouttpye = '';
    
    protected $menu     = array();
    protected $model    = '';
    protected $mainmodel= '';
    protected $data     = array();
    
    public function __construct()
    {
        parent::__construct();
        
        $this->_setClass()->_setUriClass()->_setMenu();
        
        $this->_checkLogin();
        
        $this->_loadmodel();
        
        $this->_setPageTitle();
        
        $this->_resetPageFilter();
    }
    
    public function index() 
    {
        $this->lst();
    }
    
    /**
    * Used to check login state
    */
    private function _checkLogin() 
    {
        if( !isLogin() and $this->class != 'login' ) 
        {
            redirect($this->config->item('gate_url') . '/login');
        }
    }
    
    /**
    * Used to set page title
    */
    private function _setPageTitle() 
    {
        if ( !isset($this->data['pagetitle']) )
            $this->data['pagetitle'] = $this->config->item('sim_name') . ' | ' . ucwords($this->class);
    }
    
    /**
    * Used to initialize layout
    * @param String view
    * @return Object this
    */
    private function _initLayout($view) 
    {
        $layout = $this->config->item('layout');
        
        foreach ( $layout as $key => $value )
        {
            $this->layout[$key]['directory'] = $value['directory'];        
            $this->layout[$key]['main'] = $value['directory'] . $value['main'];        
            
            foreach ( $value['items'] as $k => $val )
            {
                if ( $val == 'content' )
                {
                    if( file_exists(APPPATH . 'views/' . $view . EXT) )
                    {
                        $this->layout[$key]['items'][$val] = $view;    
                    }
                    else
                    {
                        $this->layout[$key]['items'][$val] = $value['directory'] . $view;
                    }
                }
                else
                {
                    $this->layout[$key]['items'][$val] = $value['directory'] . $val;
                }
            }
        }
        
        return $this;
    }
    
    /**
    * 
    * Used to initialize asset of layout
    * @param String layoutdir
    * @return Object this 
    */    
    private function _initLayoutAssets($layoutdir)
    {
        if( is_dir( APPPATH . 'views/' . $layoutdir . 'assets' ) )
            $this->data['layout_path'] = base_path() . '/views/' . $layoutdir . 'assets/';
        else               
            $this->data['layout_path'] = base_assets();
            
        return $this;   
    }
    
    /**
    * Used to setting class name 
    * @return Object this 
    */
    private function _setClass()
    {
        $this->class = !empty($this->uri->segment(1)) ? $this->uri->segment(1) : strtolower(get_class($this));
        
        $this->data['class'] = $this->class;
        
        return $this;    
    }
    
    /**
    * Used to setting uri of class 
    * @return Object this
    */
    private function _setUriClass()
    {
        $this->uriclass = base_url() . $this->class;
        
        $this->data['uriclass'] = $this->uriclass;    
        
        return $this;
    }
    
    private function _getMenu() 
    {
        if ( !$this->menu )
        {
            if ( $_SESSION['GATE']['menu'] )
            {
                $menu = $this->encrypt->decode($_SESSION['GATE']['menu']);
                
                $this->menu = getJSON($menu);
            }    
        }
    }
    
    private function _setMenu($level=1, $first = true) 
    {
        $this->_getMenu();
        
        if ( $this->menu )
        {
            switch($level){
                case 1 :
                    $html .= "<ul id='mainmenu' class='sf-menu'>";       
                    break;
                case 2 :
                    $html .= "<ul class='sf-menu'>";
                    break;  
            }
            
            if($first)
                $html .= "<li><a href='".site_url($this->config->item('app_path')."main")."'>Home</a></li>";

            if(isset($this->menu))
            {
                foreach($this->menu as $key => $value)
                {
                    if( !in_array($key, $this->hidemenu) ) 
                    {
                        if( !empty($this->menu[$key]['sub']) and $value['isactive'] == 1)
                        {
                            $html .= "<li><a href='javascript:void(0)'>" . $value['name'] . "</a>";
                            
                            $html .= $this->_setMenu($value['sub'],($value['level']+1),false);
                            
                            $html .= "</li>";
                        }
                        elseif($value['isactive'] == 1 and $value['isread'] == 1)
                        {
                            $html .= "<li><a href='".site_url($this->config->item('app_path'))."$value[url]'>$value[name]</a></li>";    
                        } 
                    }      
                }    
            }
            $html .= "</ul>";
        }
        
        $this->data['htmlmenu'] = $html;
        
        return $this;    
    }
    
    /**
    * Used to load main model
    */
    private function _loadmodel() 
    {
        $mainmodel = '';
        
        if( !empty($this->mainmodel) )
        {
            $this->load->model('model_' . $this->mainmodel, $this->mainmodel);
            
            $mainmodel = $this->mainmodel;
            
            $this->model = $this->$mainmodel;
        }

        return $this;
    }
    
    /**
    * Used to build view
    * @return Object this 
    */
    protected function build() 
    {
        $this->load->library('view');
        
        // set layout
        $this->view->layout = $this->layout[$this->layouttpye]['main'];
        // set data layout
        $this->view->data($this->data);
        // load view items
        $this->view->load($this->layout[$this->layouttpye]['items']);
        //rendering
        $this->view->render();
        
        return $this;    
    }
    
    /**
    * Used to initialize of view
    * @param String template
    * @param String layouttype
    * @return Object this 
    */
    protected function initview($template, $layouttype = 'list')
    {
        // set variable
        $this->template = $template;
        $this->layouttpye = $layouttype;
        
        // init layout
        $this->_initLayout($this->template);
        
        // init asset layout
        $this->_initLayoutAssets($this->layout[$this->layouttpye]['directory']);
        
        return $this;
    }
    
    /**
    * Used to show list 
    * @param Integer offset
    */
    protected function lst($offset = 1) 
    {
        if ( !$this->data['cread'] )
            $this->accessDeny();
        
        # action
        if ( $this->input->post() ) 
        {            
            $post = $this->input->post();
            
            if ( !empty($post['q']) ) 
            {
                $this->_addPageFilter($post['q']);
                
                $this->data['arr_page_filter'] = $this->_getPageFilter();
                
                foreach ($this->data['arr_kolom'] as $row) 
                {
                    foreach ( $this->data['arr_page_filter'] as $key => $filter )
                        $this->model->addFilter("lower($row[kolom]) = '". strtolower($filter) ."'", 'or');
                }
            }    
        }
        
        if ( !isset($this->data['arr_data']) ) 
        {
            $this->data['key'] = $this->model->getKey(); 
            $this->data['separator'] = $this->model->key_separator; 
            $this->data['arr_data'] = $this->model->getRows(true);
        }
        
        $this->build();
    }
    
    /**
    * Used to show detail 
    * @param Integer offset
    */
    protected function detail($key)
    {
        if ( !$this->data['cedit'] )
            $this->accessDeny();
    }
    
    /**
    * Used to show edit 
    * @param Integer offset
    */
    protected function edit($key)
    {
        if ( !$this->data['cedit'] )
            $this->accessDeny();
    }
    
    /**
    * Used to set access deny of view
    */
    protected function accessDeny() 
    {
        $this->layout[$this->layouttpye]['items']['content'] = $this->layout[$this->layouttpye]['directory'] . 'accessdeny';
    }
    
    /**
    * Used to set message
    * @param Integer status
    * @param String message
    * @param Integer errorcode
    */
    protected function displayMessage($status, $message, $errorcode) 
    {
        switch ( $status ) {
            case 0 :
                $this->session->set_flashdata('notification_error', $message);
            break;
            case 1 :
                $this->session->set_flashdata('notification_warning', $message);
            break;
            case 2 :
                $this->session->set_flashdata('notification_info', $message);
            break;
        }
    }
    
    protected function _addPageFilter($keystring) 
    {
        $_SESSION[SIM][$this->class]['page_filter'][] = $keystring;    
    }
    
    protected function _getPageFilter() 
    {
        return $_SESSION[SIM][$this->class]['page_filter'];
    }
    
    protected function _resetPageFilter() 
    {
        if ( isset($_SESSION[SIM][$this->class]['page_filter']) )
        {
            foreach ($_SESSION[SIM] as $key => $val) 
            {
                if ( $key != $this->class )
                    unset( $_SESSION[SIM][$this->class]['page_filter'] );    
            }
        }
    }
}
?>
