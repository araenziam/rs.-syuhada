<?php
require_once( BASEPATH . 'helpers/file_helper.php' );
require_once( BASEPATH . 'libraries/Parser.php' );

class Base_Parser extends CI_Parser 
{
    public $l_delim = '{';
    public $r_delim = '}';
    public $object;
    
    /**
    * Parse a template
    *
    * Parses pseudo-variables contained in the specified template view,
    * replacing them with the data in the second param
    *
    * @access    public
    * @param    string
    * @param    array
    * @param    bool
    * @return    string
    */
    public function parse_text($template, $data)
    {
        if ( file_exists($template) )
        {
            $str_output = read_file($template);
            
            return $this->_parse($str_output, $data, true);
        } 
        else
            die('File cannot be read');
        }   
    }
?>
