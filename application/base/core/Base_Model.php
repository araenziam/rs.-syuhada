<?php
require_once( BASEPATH . 'core/Model.php' );

class Base_Model extends CI_Model 
{
    protected $schema       = '';
    protected $table        = '';
    protected $key          = '';    
    protected $value        = '';
    protected $order        = '';
    protected $limit        = 10;
    protected $offset       = '';
    
    private $str_query      = '';
    private $str_clausa     = '';
    private $iscombo        = false;

    public $key_separator   = '_';    
    public $arr_filter      = array();
    public $arr_query       = array();
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    /**
    * Mendapatkan nama table
    * @param $str_table
    * @return String table 
    */
    public function getTable($str_table = '') 
    {
        $table = !empty($str_table) ? $str_table : $this->table;
        
        $table = !empty($this->schema) ? $this->schema . '.' . $table : $table;
        
        return $table;
    }
    
    /**
    * Mendapatkan key pada table
    * @return String key 
    */
    public function getKey() 
    {
        if ( is_array($this->key) ) 
        
            return implode($this->key_separator, $this->key);
            
        elseif ( count(explode($this->key_separator, $this->key)) > 0 )
        
            return str_replace(',', $this->key_separator, $this->key);  
                  
        else
        
            return $this->key;   
    }
    
    /**
    * Mendapatkan value pada table
    * @return String key 
    */
    public function getValue() 
    {
        if ( is_array($this->value) ) 
        
            return implode($this->key_separator, $this->value);
            
        elseif ( count(explode(',', $this->value)) > 0 )
        
            return str_replace(',', $this->key_separator, $this->value);  
                  
        else
        
            return $this->value;   
    }
    
    /**
    * Mendapatkan order pada table
    * @return String order 
    */
    public function getOrder() 
    {
        $str_order = array();
        
        if ( is_array($this->order) ) 
        {
            foreach ( $this->order as $key => $value ) 
            {
                if ( is_numeric($key) ) 
                    $str_order[] = $value . ' asc';
                else
                    $str_order[] = $key . ' ' . $value;                 
            } 
                
            return implode(', ', $str_order);      
        }
        else {
            return $this->order;
        }
    }
    
    /**
    * Mendapatkan base query
    * @return String str_query 
    */
    protected function baseQuery() 
    {
        $this->str_query = "select * from " . $this->getTable();
        
        return $this->str_query;
    }
    
    /**
    * Set base query
    * @return String str_query 
    */
    public function setQuery($str_query) 
    {
        $this->_resetQuery();
        
        $this->str_query = $str_query;
        
        return $this;
    }
    
    /**
    * Mendapatkan query
    * @return String str_clausa 
    */
    public function getQuery() 
    {
        $this->str_query = $this->baseQuery() . ' ' . $this->getClausa() . ' ' . $this->getLimit();
        
        return $this->str_query;
    }
    
    /**
    * Mereset query
    * @return String str_clausa 
    */
    private function _resetQuery() 
    {
        $this->str_query = null;
        $this->arr_filter = array();
        $this->str_clausa = null;
    }
   
    /**
    * Mendapatkan clausa
    * @return String str_clausa 
    */ 
    public function getClausa() 
    {
        if ( count($this->getFilter()) > 0 ) 
        {
            $this->str_clausa = 'where ' . implode(' ', $this->getFilter());
        }
        
        return $this->str_clausa;
    }
    
    /**
    * Set Clausa
    * @return String str_clausa 
    */
    public function setClausa($str_clausa) 
    {
        if ( strpos(strtolower(trim($this->str_clausa)),'where') !== FALSE)
            $this->str_clausa = $str_clausa;
        else
            $this->str_clausa = 'where ' . $str_clausa;
        
        return $this;    
    }
    
    /**
    * Mereset clausa
    * @return String str_clausa 
    */
    private function _resetClausa() 
    {
        $this->str_clausa = null;
    }
    
    /**
    * Menambahkan filter
    */
    public function addFilter($str_filter = '', $str_filter_type = 'and')
    {
        array_push($this->arr_filter, $str_filter_type . ' ' . $str_filter);
        
        return $this;
    }
    
    /**
    * Mendapatkan filter
    * @return String key 
    */
    public function getFilter() 
    {
        foreach ( $this->arr_filter as $key => $val ) 
        {
            if ( $key == 0 ) 
            {
                $this->arr_filter[$key] = ltrim(ltrim(ltrim($val, 'or'), 'and'));
            }    
        }
        return $this->arr_filter;
    }
    
    public function getCondition($key, $row)
    {
        $a_key = explode($this->key_separator, $key);

        foreach ( $a_key as $key => $val )
            $this->addFilter("$val = '$row[$val]'");
            
        return $this->getClausa();
    }
    
    /**
    * Set Limit
    * @return String key 
    */
    public function setLimit($int_limit = 0, $int_offset = 0) 
    {
        $this->limit = $int_limit;
        
        $this->offset = (($int_offset - 1) * $this->limit);
        
        return $this;        
    }
    
    /**
    * Mendapatkan limit
    * @return String key 
    */
    public function getLimit() 
    {
        if ( $this->limit and $this->offset )
            return "limit " . $this->offset . ',' . $this->limit;
        elseif ( $this->limit )
            return "limit " . $this->limit; 
        else
            return "";
    }
    
    /**
    * Mendapatkan field
    * @return Array field 
    */
    public function getField() 
    {
        if ( $this->getTable() )
            $arr_fields = $this->db->list_fields($this->getTable());
        else
            $arr_fields = array();
            
        return $arr_fields;
    }
    
    /**
    * Mendapatkan data row
    * @return Array field 
    */
    public function getRow() 
    {
        $sql = $this->getQuery();

        $arr_data = $this->db->query($sql)->row_array();
        
        $this->arr_query[] = $this->db->last_query();
        
        $this->_resetQuery();
        
        return $arr_data;        
    }
    
    /**
    * Mendapatkan data row
    * @return Array field 
    */
    public function getRows($showtotal = true, $offset = 1) 
    {
        $int_total = 0;
        $arr_data = array();
        
        if ( !$this->iscombo )
            $this->setLimit($this->limit, $offset);
        else
            $this->setLimit(0, 0);
            
        $sql = $this->getQuery();
        
        // get data
        if ( $this->db->query($sql) ) 
        {
            $arr_data = $this->db->query($sql)->result_array();
            
            $this->arr_query[] = $this->db->last_query();
            
            //get total
            $int_total = $this->db->query($sql)->num_rows();
        }
        //reset query
        $this->_resetQuery();
        
        if ( $showtotal )
        {
            $arr_result['data'] = $arr_data;
            $arr_result['total'] = $int_total;
        }
        else {
            $arr_result = $arr_data;
        }
        
        return $arr_result;        
    }
    
    /**
    * Mendapatkan combo
    * @return Array result 
    */
    public function getCombo($bol_showboth = true, $str_filter = '') 
    {
        $this->iscombo = true;
        
        if ( !empty($str_filter) ) 
            $this->setClausa($str_filter);
            
        $arr_key = explode($this->key_separator, $this->getKey());
        
        $arr_value = explode($this->key_separator, $this->getValue());
        
        $arr_data = $this->getRows(false);                       
        
        $arr_result = array();
        
        foreach ( $arr_data as $row ) 
        {
            $v_key = array();
            foreach ( $arr_key as $key ) 
                $v_key[] = $row[$key];
            
            $v_value = array();    
            foreach ( $arr_value as $value ) 
                $v_value[] = $row[$value];   
            
            if ( $bol_showboth )
                $arr_result[implode($this->key_separator, $v_key)] = implode(' - ', $v_value);
            else
                $arr_result[implode($this->key_separator, $v_key)] = implode($this->key_separator, $v_key);
        }
        
        return $arr_result;
    }
    
    function update($data, $filter) 
    {
        if( $data )
        {
            $sql = 'update '  . $this->getTable() . ' set ';
            
            foreach ( $data as $key => $value )
            {
                if ( is_numeric($value) ) 
                {
                    $a_sql[] = "$key = $value ";    
                }
                else {
                    $a_sql[] = "$key = '$value' ";
                }
            }
            
            $sql .= implode(', ', $a_sql) . $filter;
        }
        
        $this->db->query($sql);
    }
}
?>
