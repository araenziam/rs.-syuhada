<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class {classname} extends MY_Model
{
    protected $schema   = '{schema}';
    protected $table    = '{tablename}';
    protected $key      = '{key}';
    
    public function __construct() 
    {
        parent::__construct();
    }
}

?>