<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_page extends MY_Model
{
    protected $schema   = 'gate';
    protected $table    = 'sys_page';
    protected $key      = '{key}';
    
    public function __construct() 
    {
        parent::__construct();
    }
}

?>