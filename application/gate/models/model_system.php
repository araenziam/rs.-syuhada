<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_system extends MY_Model
{
    protected $schema   = 'gate';
    protected $table    = 'sys_system';
    protected $key      = '{key}';
    
    public function __construct() 
    {
        parent::__construct();
    }
}

?>