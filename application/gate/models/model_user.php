<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user extends MY_Model
{
    protected $schema   = 'gate';
    protected $table    = 'sys_user';
    protected $key      = 'iduser';
    
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function login($username, $password) 
    {
        $this->addFilter("username = '$username'");
        $row = $this->getRow();

        if ( $row ) 
        {
            if ( $row['password'] == md5(md5($password.$row['salt']).$row['salt']) ) 
            {
                $log['lastlogintime'] = date('Y-m-d G:i:s');
                $log['lastloginip'] = $this->input->ip_address();
                
                $this->update($log, $this->getCondition($this->getKey(), $row));
                
                $data['poststatus'] = 1;
                $data['postmessage'] = lang('login_verified');
                $data['postdata'] = $row;    
            }
            else {
                $data['poststatus'] = 0;
                $data['postmessage'] = lang('password_failed');
                $data['postdata'] = null;
            }
        } 
        else {
            $data['poststatus'] = 0;
            $data['postmessage'] = lang('user_not_found');
            $data['postdata'] = null;
        }

        return array($data['poststatus'], $data['postmessage'], $data['postdata']);
    }
}

?>