<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    
class Menu extends MY_Controller 
{
    public $mainmodel   = 'user';
    
    public function __construct() 
    {
        parent::__construct();
        
        $this->data['cread'] = true;
    }
    
    public function lst($page) 
    {
        parent::initview('menu', 'login');
        parent::lst($offset);
    }
    
    public function setmenu($rolemenu) 
    {
        $rolemenu = decrypt_url($rolemenu);
        
        list($system, $idunit, $koderole, $iduser) = explode('_', $rolemenu);
        
        // menu GATE 
        $arr_menu['GT'][1]['name'] = 'User'; 
        $arr_menu['GT'][1]['url'] = 'user';
        $arr_menu['GT'][1]['level'] = 1;
        $arr_menu['GT'][1]['isread'] = 1;
        $arr_menu['GT'][1]['isactive'] = 1;
        
        // menu SDM
        $arr_menu['SDM'][1]['name'] = 'Biodata';
        $arr_menu['SDM'][1]['url'] = 'biodata';
        $arr_menu['SDM'][1]['level'] = 1;
        $arr_menu['SDM'][1]['isread'] = 1;
        $arr_menu['SDM'][1]['isactive'] = 1;
        
        $menu = $arr_menu[$system];
        
        $_SESSION['GATE']['menu'] = $this->encrypt->encode(setJSON($menu));
        
        switch ( $system )
        {
            case 'GT' : redirect($this->config->item('gate_url').'/home'); break;
            case 'SDM' : redirect($this->config->item('sdm_url').'/home'); break;
        }
    }
}  
?>
