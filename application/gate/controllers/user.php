<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    
class User extends MY_Controller 
{
    public $mainmodel   = 'user';
    
    public function __construct() 
    {
        parent::__construct();
        
        $this->data['cread'] = true;
    }
    
    public function lst($page) 
    {
        // Define Page
        $this->data['pagetitle'] = "Daftar Pengguna";
         
        // Define Kolom
        $this->data['arr_kolom'][] = array('kolom' => 'username', 'label' => 'Username');
        $this->data['arr_kolom'][] = array('kolom' => 'namauser', 'label' => 'Nama Pengguna');
        $this->data['arr_kolom'][] = array('kolom' => 'lastlogintime', 'label' => 'Login Terakhir');
        $this->data['arr_kolom'][] = array('kolom' => 'lastloginip', 'label' => 'IP Login');
        
        parent::initview('list_master');
        parent::lst($offset);
    }
}  
?>
