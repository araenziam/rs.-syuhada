<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    
class Login extends MY_Controller 
{
    public $mainmodel   = 'user';
    
    public function __construct() 
    {
        parent::__construct();
        
        $this->data['cread'] = true;
    }
    
    public function lst($page = 1) 
    {
        $post = $this->input->post();
        
        if ( isset($post['username']) and isset($post['password']) )
        {
            list($poststatus, $postmessage, $postdata) = $this->model->login($post['username'], $post['password']);
            
            if ( $poststatus )
            {
                $this->_setSession($postdata);
                redirect('menu');
            }
            else {
                $this->displayMessage($poststatus, $postmessage);
                
                redirect('login');
            }
        }
                
        parent::initview('login', 'login');
        parent::lst($offset);
    }
    
    public function logout() 
    {
        unset($_SESSION[SIM]);
        
        redirect($this->config->item('gate_url') . '/login');
    }
    
    private function _setSession($data)
    {
        $_SESSION[SIM]['user']['iduser']    = $data['iduser'];
        $_SESSION[SIM]['user']['username']  = $data['username'];
        $_SESSION[SIM]['user']['nama']      = $data['nama'];
    }
}  
?>
