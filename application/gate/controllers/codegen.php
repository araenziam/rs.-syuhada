<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    
class Codegen extends CI_Controller 
{
    private $_base_parser;
    private $str_output = '';
    
    public function __construct() 
    {
        include_once( COREPATH . 'core/Base_parser.php' );
        
        $this->_base_parser = new Base_Parser();
        
        parent::__construct();
    }
    
    public function index() 
    {
        echo anchor('login');
    }
    
    public function generate($schema = '', $prefix = 'sys') 
    {
        $this->load->helper('file');
        
        $template = COREPATH . 'template/Model.inc';
        
        if( $this->db->dbdriver == 'mysql' )
        {
            $schema = !empty($schema) ? $schema : $this->db->database;    
        }
        else {
            $schema = !empty($schema) ? $schema : 'public';
        }
        
        $prefix = $prefix . '_';
        
        $arr_table = $this->db->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '$schema'");

        foreach ( $arr_table->result_array() as $table ) 
        {
            if ( !file_exists(APPPATH . 'models/model_' . strtolower(str_replace($prefix, '', $table['table_name'])) . '.php') )
            {
                if ( strpos(strtolower($table['table_name']), $prefix) !== false )
                {
                    $params['classname'] = 'Model_' . strtolower(str_replace($prefix, '', $table['table_name']));
                    $params['tablename'] = $table['table_name'];
                    $params['schema'] = $schema;
                    // not working in postgre
                    //$params['key'] = $this->_getKey($table['table_name']);
                    
                    $str_output = $this->_base_parser->parse_text($template, $params);
                    
                    write_file(APPPATH . 'models/model_' . strtolower(str_replace($prefix, '', $table['table_name'])) . '.php', $str_output);
                }
            }
        }
        
        echo $data;
    }
    
    private function _getKey($table) 
    {
        $arr_keys = array();
        $fields = $this->db->field_data($table);

        foreach ($fields as $field)
        {
            if( $field->primary_key )
            {
                $arr_keys[] = strtolower($field->name);
            }
        }
        
        if($arr_keys)
            return implode(',', $arr_keys);
        else
            return '';
    }        
}  
?>
