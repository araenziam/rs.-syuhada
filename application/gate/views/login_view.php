<div id="login" class="box">
    <h2>Login</h2>
    <section>
        <div id="loading" class="information msg">Please wait...</div>
        <div class="error msg">Failed username or password</div>
        <form id="loginform" method="post" action="<?php echo base_url().$cpanel_dir."main/login"?>">
            <dl>
                <dt><label for="username">Username</label></dt>
                <dd><input id="username" autocomplete="off" name="username" type="text" /></dd>
            
                <dt><label for="password">Password</label></dt>
                <dd><input id="adminpassword" name="password" type="password" /></dd>
            </dl>
            <p>
                <button type="submit" class="button gray" id="loginbtn">LOGIN</button>
                <a id="forgot" href="<?php echo base_url().$cpanel_dir.'main/forgot'?>">Forgot Password?</a>
            </p>
        </form>
    </section>
</div>