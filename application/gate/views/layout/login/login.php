<div id="login" class="box">
    <h2>Login</h2>
    <section>
        <?php $notification->render()?>
        <form id="loginform" method="post" action="<?= base_url()?>login">
            <dl>
                <dt><label for="username">Username</label></dt>
                <dd><input id="username" autocomplete="off" name="username" type="text" /></dd>
            
                <dt><label for="password">Password</label></dt>
                <dd><input id="adminpassword" name="password" type="password" /></dd>
            </dl>
            <p>
                <button type="submit" class="button gray" id="loginbtn">LOGIN</button>
                <a id="forgot" href="<?php echo base_url().'login/forgot'?>">Forgot Password?</a>
            </p>
        </form>
    </section>
</div>