<!DOCTYPE HTML>
<html lang="en">
<head>
<title><?php echo $pagetitle?></title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="<?= $layout_path?>css/style.css">
<!--[if lte IE 8]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="<?= $layout_path?>js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?= $layout_path?>js/cufon-yui.js"></script>
<script type="text/javascript">
$(function() {
	Cufon.replace('#site-title');
	$('.msg').click(function() {
		$(this).fadeTo('slow', 0);
		$(this).slideUp(341);
	});  
});
</script>
</head>
<body>
<header id="top">
	<div class="container_12 clearfix">
		<div id="logo" class="grid_12">
			<a id="site-title" href=""><span>Administrator</span></a>
		</div>
	</div>
</header>
