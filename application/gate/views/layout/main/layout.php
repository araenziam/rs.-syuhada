<?php $header->render()?>

<?php $menu->render()?>

<section id="content">
    <section class="container_12 clearfix">
        <section id="main" class="grid_12">
            <article>
                <h1><?php echo $pagetitle?></h1>
                
                <?php echo $notification->render()?>
                
                <form id="form" method="post" action="">                    
                    <?php $content->render();?>
                </form>
            </article>
        </section>
    </section>
</section>

<?php $footer->render()?>