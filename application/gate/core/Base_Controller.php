<?php

session_start();

require_once APPPATH.'libraries/mobiledetect/Mobile_Detect.php';

class Base_Controller extends CI_Controller
{
    public $data = array();
	protected $uri_segments = array();
	protected $ctl;
	protected $method;
	protected $model;
	protected $lstctl;
	
	protected $cache_time = 86400; // 3600 * 60 * 24 = 1 day

	protected $model_p;
	protected $detail_view;
	protected $list_view;
	protected $list_limit;

	protected $view_items = array();
	
	protected $layout_type = 'classic';
	
	# pengaturan menu
	protected $akses = array();
	protected $menu_access = array();
	protected $menu_unserialize = array();
	
	public $auth = array();
	
	# debug sql hooks
	public $list_sql = array();

	# ora messages
	public $ora_messages = array();

    public function __construct()
    {
        parent::__construct();

		$this->load->helper('language');
		$this->lang->load('general', 'id');
		$this->data['notification'] = $this->session->flashdata('notification');
		
		$this->data['page_title'] = lang('page_title');		
		
		$_SESSION[G_SESSION]['loginrole']=$this->CekRoleUserLogin();

		$this->uri_segments = $this->uri->uri_to_assoc(3);

		$this->ctl = $this->router->fetch_class();
		$this->data['ctl'] = $this->ctl;
		
		$this->method = $this->router->fetch_method();
		$this->data['method'] = $this->method;
		
		$this->lstctl = $this->ctl.'/lst';
		
		//$this->initLayoutType();

		$free_classes = array('login','logout','ajax','cron','publ1c','error','gate','test','download');
		if (in_array($this->router->class, $free_classes) or $this->free_class) {
			$this->data['c_create'] = true;
			$this->data['c_read'] = true;
			$this->data['c_update'] = true;
			$this->data['c_delete'] = true;
			
			$this->auth = array('c_read','c_create','c_update','c_delete');
			
			$this->data['with_form'] = true;
			
			return true;
		}
		$this->loadSetting();

        /*
		# sementara dibuka semua
		$this->data['c_create'] = true;
		$this->data['c_read'] = true;
		$this->data['c_update'] = true;
		$this->data['c_delete'] = true;
		
		$this->auth = array('c_read','c_create','c_update','c_delete');
        */
        		
		if (!xIsUserLoggedIn()) {
			$_SESSION[G_SESSION]['url_redirect'] = current_url();
			redirect($this->config->item('gate_login_url'));
		}
			
		if (!isset($_SESSION[G_SESSION]['CREATED'])) {
			$_SESSION[G_SESSION]['CREATED'] = time();
		}
		else if (time() - $_SESSION[G_SESSION]['CREATED'] > 1800) {
			session_regenerate_id(true);    // change session ID for the current session an invalidate old session ID
			$_SESSION[G_SESSION]['CREATED'] = time();  // update creation time
		}
        
		# unset variabel menu setelah selesai
		unset($this->menu_access);
		unset($this->max_level_menu);
		unset($this->menu_parent);
		unset($this->menu_parent_level);
		
		$this->load->helper('html');
		
		$this->load->database();

		$this->_setMenuRelated();
		$this->data['menus'] = 	"<ul>\n" . $this->_setMenu() . "</ul>\n";

		$this->checkRoleAuth();
		
		//$this->_updateSessionData();
		
		$this->data['with_form'] = true;
		
		// file help
		$helpfile = $this->ctl.'_'.$this->method.'.pdf';
		// $assetspath = $this->config->item('assets_path');
		
		$assetspath = $_SERVER['SCRIPT_FILENAME'];
		$pos = strrpos($assetspath,'/');
		$assetspath = substr($assetspath,0,$pos).'/application/akad/assets/';
		
		if(!file_exists($assetspath.'help/'.$helpfile))
			$helpfile = 'main.pdf';
		
		$this->data['helpfile'] = $helpfile;
		
		// combo sistem kuliah dan kampus
		if(!xIsMahasiswa()) {
			$this->load->model('kampus_model','Kampus');
			$this->load->model('sistemkuliah_model','SistemKuliah');
			
			$a_kampus = $this->Kampus->getListCombo(true,true,true);
			$a_sistem = $this->SistemKuliah->getListCombo(true,true);
			
			$this->data['pubkampus'] = xGetPostValue('public','kampus',$this->input->post('pubkampus'),$a_kampus);
			$this->data['pubsistem'] = xGetPostValue('public','sistem',$this->input->post('pubsistem'),$a_sistem);
			
			$this->data['a_pubkampus'] = $a_kampus;
			$this->data['a_pubsistem'] = $a_sistem;
			
			$this->data['menu_lokasi'] = true;
		}

		//$this->validateSession();
    }

	public function index() {
	 
		redirect("$this->lstctl");
	}
	public function loadSetting(){
			#load dan buat session setting
		$this->load->model('setting_model','Setting');
		$this->load->model('periode_model','Periode');
		
		$datasetting=$this->Setting->getDataSetting();
		$_SESSION[G_SESSION]['set_kurikulumberlaku']=$datasetting['THNKURIKULUMSEKARANG'];
		$_SESSION[G_SESSION]['set_periodesekarang']=$datasetting['PERIODESEKARANG'];
		$_SESSION[G_SESSION]['set_periodesp']=$datasetting['PERIODESP'];
		$_SESSION[G_SESSION]['set_periodekrsta']=$datasetting['PERIODEKRSTA'];
		$_SESSION[G_SESSION]['set_periodenilai']=$datasetting['PERIODENILAI'];
		
		//$_SESSION[G_SESSION]['set_batassksdefault']=$datasetting['BATASSKSDEFAULT'];
		//$_SESSION[G_SESSION]['set_nangkatutup']=$datasetting['NANGKATUTUP'];
		# tanggal periode uts, uas, susulan, kuliah
/*		$_SESSION[G_SESSION]['set_periodekuliahmulai']=$datasetting['TGLKULIAHMULAI'];
		$_SESSION[G_SESSION]['set_periodekuliahakhir']=$datasetting['TGLKULIAHAKHIR'];
		$_SESSION[G_SESSION]['set_periodeutsmulai']=$datasetting['TGLUTSMULAI'];
		$_SESSION[G_SESSION]['set_periodeutsakhir']=$datasetting['TGLUTSAKHIR'];
		$_SESSION[G_SESSION]['set_periodeuasmulai']=$datasetting['TGLUASMULAI'];
		$_SESSION[G_SESSION]['set_periodeuasakhir']=$datasetting['TGLUASAKHIR'];
		$_SESSION[G_SESSION]['set_periodesusulanutsmulai']=$datasetting['TGLSUSULANUTSMULAI'];
        $_SESSION[G_SESSION]['set_periodesusulanutsakhir']=$datasetting['TGLSUSULANUTSAKHIR'];
        $_SESSION[G_SESSION]['set_periodesusulanuasmulai']=$datasetting['TGLSUSULANUASMULAI'];
        $_SESSION[G_SESSION]['set_periodesusulanuasakhir']=$datasetting['TGLSUSULANUASAKHIR'];
		$_SESSION[G_SESSION]['set_pesanpengesahan']=$datasetting['PESANPENGESAHAN'];
*/
		$_SESSION[G_SESSION]['set_namaperiodesekarang']=$this->Periode->getNamaPeriode($datasetting['PERIODESEKARANG']);
		
		$semester = (int)substr($datasetting['PERIODESEKARANG'],-1);
		if($semester == 1 or $semester == 3)
			$_SESSION[G_SESSION]['set_periodereguler']=$datasetting['PERIODESEKARANG'];
		else
			$_SESSION[G_SESSION]['set_periodereguler']=substr($datasetting['PERIODESEKARANG'],0,4).($semester-1);
	}
	public function lst($offset, $mode='NOLEFT') {
		if (!$this->data['c_read'])
			$this->toNoData();
		
		// filter all
		if(!empty($_SESSION[G_SESSION][$this->ctl]['filter_all']) and !empty($this->data['field_list']))
			$this->initFilterAll();
		
		// cek cetak
		if($this->input->post("act") == "cetak") {
			$format = $this->input->post("format");
			$this->data['format'] = $format;
			
			switch($format) {
				case 'doc' :
					header("Content-Type: application/msword");
					header('Content-Disposition: attachment; filename="'.$this->ctl.'.doc"');
					break;
				case 'xls' :
					header("Content-Type: application/msexcel");
					header('Content-Disposition: attachment; filename="'.$this->ctl.'.xls"');
					break;
			}
			
			$this->data['rows'] = $this->model->getList();
			
			if(empty($this->print_view))
				$this->renderView('_list_cetak',true);
			else
				$this->renderView($this->print_view,true);
		}
		else {
			if(!$this->search_list or $this->search) {
				if (!$_SESSION[G_SESSION][$this->ctl]['filter_num_record'])
					$_SESSION[G_SESSION][$this->ctl]['filter_num_record'] = $this->list_limit;
				else
					$this->list_limit = $_SESSION[G_SESSION][$this->ctl]['filter_num_record'];
				
				$rows = $this->model->getList($this->list_limit, $offset);
				
				if($this->model->ids) {
					foreach ($rows as &$row) {
						$pk_str = '';
						foreach ($this->model->getID() as $pk) {
							if ($pk_str)
								$pk_str .= "/";
							$pk_str .= $row[$pk];
						}
						$row[$this->model->ids] = $pk_str;
					}
				}
				else if (is_array($this->model->getID())) {
					$sep = $this->config->item('id_separator');
					$pks = implode($sep, $this->model->getID());
					foreach ($rows as &$row) {
						$pk_str = '';
						foreach ($this->model->getID() as $pk) {
							if ($pk_str)
								$pk_str .= $sep;
							$pk_str .= "{$pk}:{$row[$pk]}";
						}
						$row[$pks] = $pk_str;
					}
				}
				
				$this->data['rows'] = $rows;
				
				$this->data['num_start'] = ($offset)+1;
				$this->data['num_end'] = ($offset)+count($this->data['rows']);
				$this->data['num_all'] = $this->model->getCount();
				
				$_SESSION[G_SESSION][$this->ctl]['offset'] = $offset;

				$this->renderPagination(site_url("$this->lstctl"), $this->data['num_all'], $offset, $this->list_limit);
				
				if($this->search_list)
					$this->data['search'] = true;
			}
			else if($this->search_list) {
				// mode search dan belum searching
				$this->data['buttons'] = array('add');
				$this->data['with_search'] = false;
			}
			
			if($this->search_list)
				$this->data['search_list'] = true;
			
			$this->data['lstctl'] = $this->lstctl;
			
			$this->renderView($this->list_view);
		}
	}
	
	public function add() {
		if (!$this->data['c_create'])
			$this->toNoData();
        $this->edit();        
    }
	
	/*
	 *
	@author
	@param
	 *
	 */
	
	public function edit($record, $id, $set=false, $redirect=true) {
		if ($this->input->post('act') === 'save') {
			if ($id) {
				$ok = $this->model->update($record, $id);
				if ($ok) {
					$notification = $this->_notification(lang('data_updated'), 'alert-success');	
					$this->session->set_flashdata('notification', $notification);
					
					if($redirect) {
						if ($set==true){
						redirect("$this->lstctl");
						}
						else{
						redirect("$this->ctl/detail/$id");					
						}
					}
				}
				else {
					// $this->data['row'] = $record;
					$this->session->set_flashdata('row', $this->input->post());
					
					$messages = lang('data_updated_fail');
					if (count($this->ora_messages)) {
						foreach ($this->ora_messages as $message) {
							$messages .= '<br/>' . $message;
						}
						
					}
					$notification = $this->_notification($messages, 'alert-error');	
					$this->session->set_flashdata('notification', $notification);
					
					if($redirect) {
						if ($set==true){
						redirect("$this->lstctl");
						}
						else{
						redirect("$this->ctl/edit/$id");					
						}
						//redirect("$this->ctl/edit/$id");
					}
				}
			}
			else {
				if (!$this->data['c_create'])
					$this->toNoData();
				$id = $this->model->add($record);
				if ($id) {
					$notification = $this->_notification(lang('data_added'), 'alert-success');	
					$this->session->set_flashdata('notification', $notification);
					
					if($redirect) {
						if ($set==true){
						redirect("$this->lstctl");
						}
						else{
						redirect("$this->ctl/detail/$id");					
						}
						//redirect("$this->ctl/detail/$id");
					}
				}
				else {
					// $this->data['row'] = $record;
					$this->session->set_flashdata('row', $this->input->post());
					
					$messages = lang('data_added_fail');
					if (count($this->ora_messages)) {
						$messages .= '<br/>' . $this->ora_messages[0];
					}
					$notification = $this->_notification($messages, 'alert-error');	
					$this->session->set_flashdata('notification', $notification);
					
					if($redirect) {
						if ($set==true){
						redirect("$this->lstctl");
						}
						else{
						redirect("$this->ctl/add");					
						}
						//redirect("$this->ctl/add");
					}
				}
			}
			
			if(empty($redirect)) {
				if(!isset($ok))
					$ok = $id;
				
				return $ok;
			}
		}
				
		$this->data['edited'] = true;
		$this->renderView($this->detail_view);
	}
	
	public function edit2($record, $ids, $set=false, $arrid=false) {
	 
		$sep = $this->config->item('id_separator');
		$vals = explode($sep, $ids);
		foreach ($vals as $val) {
			$temp = explode(':', $val);
			$arr[$temp[0]] = $temp[1];
		}

		if ($this->input->post('act') === 'save') {
			if ($ids) {
				if ($this->model->update2($record, $arr)) {
					$notification = $this->_notification(lang('data_updated'), 'alert-success');	
					$this->session->set_flashdata('notification', $notification);

					if (is_array($this->model->getID($arrid))) {
						$sep = $this->config->item('id_separator');
						$pk_str = '';
						foreach ($arr as $k=>$v) {
							if ($pk_str)
								$pk_str .= $sep;
							$pk_str .= "{$k}:{$v}";
						}
					}
					if ($set==true){
					redirect("$this->lstctl");
					}
					else{
					if($this->model->ids)
						$pk_str = $this->model->setDataID($record);
					redirect("$this->ctl/detail/$pk_str");
					//redirect("$this->ctl/detail/$id");					
					}
					//redirect("$this->ctl/detail/$pk_str");					
				}
				else {
					// $this->data['row'] = $record;
					$this->session->set_flashdata('row', $this->input->post());
					
					$messages = lang('data_updated_fail');
					if (count($this->ora_messages)) {
						foreach ($this->ora_messages as $message) {
							$messages .= '<br/>' . $message;
						}
						
					}
					$notification = $this->_notification($messages, 'alert-error');	
					$this->session->set_flashdata('notification', $notification);

					if (is_array($this->model->getID($arrid))) {
						$sep = $this->config->item('id_separator');
						$pk_str = '';
						foreach ($arr as $k=>$v) {
							if ($pk_str)
								$pk_str .= $sep;
							$pk_str .= "{$k}:{$v}";
						}
					}
					if ($set==true){
					redirect("$this->lstctl");
					}
					else{
					if($this->model->ids) {
						$arrid = $this->model->getDataArrID($ids);
						$pk_str = $this->model->setDataID($arrid);
					}
					redirect("$this->ctl/edit/$pk_str");
					//redirect("$this->ctl/detail/$id");					
					}
					//redirect("$this->ctl/detail/$pk_str");					
				}
			}
			else {
				if (!$this->data['c_create'])
					$this->toNoData();

				$pks = $this->model->add2($record);
				if ($pks) {
					$notification = $this->_notification(lang('data_added'), 'alert-success');	
					$this->session->set_flashdata('notification', $notification);
					
					if (is_array($this->model->getID($arrid))) {
						$pk_str = '';
						foreach ($pks as $k=>$v) {
							if ($pk_str)
								$pk_str .= $sep;
							$pk_str .= "{$k}:{$v}";
						}
					}
					if ($set==true){
					redirect("$this->lstctl");
					}
					else{
					if($this->model->ids)
						$pk_str = $this->model->setDataID($record);
					redirect("$this->ctl/detail/$pk_str");
					//redirect("$this->ctl/detail/$id");					
					}
					//redirect("$this->ctl/detail/$pk_str");					
				}
				else {
					// $this->data['row'] = $record;
					$this->session->set_flashdata('row', $this->input->post());
					
					$messages = lang('data_added_fail');
					if (count($this->ora_messages)) {
						$messages .= '<br/>' . $this->ora_messages[0];
					}
					$notification = $this->_notification($messages, 'alert-error');	
					$this->session->set_flashdata('notification', $notification);
					redirect("$this->ctl/add");
				}
			}
		}
				
		$this->data['edited'] = true;
		$this->renderView($this->detail_view);
	}

	public function delete($id) {
		if (!$this->data['c_delete'])
			$this->toNoData();
		
		// cek model
		$sep = $this->config->item('id_separator');
		if($this->model->ids) {
			$aid = explode($sep,$this->model->ids);
			$method = func_get_arg(count($aid));
			
			$args = func_get_args();
			if(!empty($method))
				array_pop($args);
			
			$id = $this->model->getDataIDExt($this->model->ids,$args);
		}
		else if(is_array($this->model->id)) {
			$method = func_get_arg(1);
		}
		else {
			$aid = explode($sep,$this->model->id);
			$method = func_get_arg(count($aid));
		}
		
		if (strstr($id, $sep)) {
			$arr = array();
			
			$vals = explode($sep, $id);
			foreach ($vals as $val) {
				$temp = explode(':', $val);
				$arr[$temp[0]] = $temp[1];
			}
			$ok = $this->model->delete2($arr);
		}
		else
			$ok = $this->model->delete($id);

		if ($ok) {
			$notification = $this->_notification(lang('data_deleted'), 'alert-success');	
			$this->session->set_flashdata('notification', $notification);
			
			if(empty($method))
				$this->backToList();
			else
				redirect("$this->ctl/$method");
		}
		else {
			$notification = $this->_notification(lang('data_deleted_fail'), 'alert-error');	
			$this->session->set_flashdata('notification', $notification);
			
			if(empty($method))
				redirect("$this->ctl/detail/$id");
			else
				redirect("$this->ctl/$method");
		}
	}
	
	public function detailSet($id,$edit=false,$cekunit=true) {
		if (!$this->data['c_read'])
			$this->toNoData();
		
		if(($edit and !((empty($id) and $this->data['c_create']) or (!empty($id) and $this->data['c_update']))) or (!$edit and empty($id)))
			$this->toNoData();
		
		if ($id) {
			$this->data['key'] = $id;
			
			$sep = $this->config->item('id_separator');
			if (strstr($id, $sep)) {
				$arr = array();
				
				$vals = explode($sep, $id);
				foreach ($vals as $val) {
					$temp = explode(':', $val);
					$arr[$temp[0]] = $temp[1];
				}
				$this->data['row'] = $this->model->getRow($arr);
			}
			else 
				$this->data['row'] = $this->model->getRow($id);
			
			//if (!$this->data['row'])
				//$this->toNoData();
		}
		
		// cek flash data
		$flash = $this->session->flashdata('row');
		if(!empty($flash)) {
			// pastikan huruf besar :D
			$uflash = array();
			foreach($flash as $k => $v)
				$uflash[strtoupper($k)] = $v;
			
			$this->data['row'] = $uflash;
		}
		
		// cek unit, eksperimental
		if($cekunit and !xCekUnit($this->data['row']))
			$this->toNoData();
		
		// untuk view
		$this->data['filter_input'] = false;
	}
	// ada perubahan
	public function detail($id) {
		if($this->model->ids)
			$id = $this->model->getDataIDExt($this->model->ids,func_get_args());
		
		$this->detailSet($id);
		
		$this->data['edited'] = false;
		$this->renderView($this->detail_view);
	}
	
	protected function backToList($param=null) {
		if(empty($_SESSION[G_SESSION][$this->ctl.'_back']))
			$lst = $this->lstctl;
		else
			$lst = $_SESSION[G_SESSION][$this->ctl.'_back'];
		
		if($offset)
			redirect($lst.'/'.$param);
		else
			redirect($lst);
	}
	
	protected function setToList($ctl=null,$method=null) {
		if(empty($ctl))
			$ctl = $this->ctl;
		if(empty($method))
			$method = $this->method;
		
		$_SESSION[G_SESSION][$ctl.'_back'] = $this->ctl.'/'.$method;
	}
	
	protected function setPostRecord($post,$row,$log=true) {
		// ambil data kolom
		if(empty($row))
			$row = $this->model->getListColumn();
		
		$record = array();
		foreach($row as $k => $v) {
			$k = strtolower($k);
			
			// skip yang tidak dipost
			if(!isset($post[$k]))
				continue;
		
			$record[$k] = dbStrNull($post[$k]);
		}
		
		// tambahan log
		if($log)
			$this->setLogRecord($record);
		
		return $record;
	}
	
	protected function getActLog() {
		return $this->ctl.'/'.$this->method;
	}
	
	protected function setLogRecord(&$record) {
		$record += dbGetRecordLog($this->getActLog());
	}
	
	protected function _notification($message, $class='alert-success') {
		$ret = "
			<div class=\"alert $class\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
			$message
		</div>";
		return $ret;
	}

	protected function _setMenuRelated() {
		$this->load->model('gate_model','GATE');
		$this->menu_unserialize = $this->GATE->getMenus();
		$this->akses = $this->GATE->getAkses();

		$koderole = $_SESSION[G_SESSION]['koderole'];
		foreach ($this->akses as $k=>$rs) {
			foreach ($rs as $idunit=>$row) {
				if ($k == $koderole) {
					$idmenu = $row['IDMENU'];
					$this->menu_access[$idmenu] = $row;
				}
			}
		}
	}

	private function _setMenu($idmenuparent=NULL) {
		$ret = '';

		foreach ($this->menu_unserialize as $menu) {
			if ($menu['IDMENUPARENT'] != $idmenuparent)
				continue;

			if (!$menu['ISACTIVE'] || !$menu['ISTAMPIL'])
				continue;

			if ($menu['NAMAMENU'] == '---') {
				$ret .= '<li><hr/></li>';
				continue;
			}

			$has_child = $this->_menuHasChild($menu['IDMENU']);
			
			if (!$has_child && !$menu['NAMAFILE']) {
				continue;
			}
			
			if ($menu['IDMENU']) {
				if (!$this->menu_access[$menu['IDMENU']]['CANREAD']) {
					continue;
				}
			}			

			$class_li = '';
			if ($menu['LEVEL']==1 && $has_child)
				$class_li = ' class="menupop"';

			$ret .= "<li{$class_li}>";
			
			$label = $menu['NAMAMENU'];
			if ($has_child)
				$label = "<span>{$menu['NAMAMENU']}</span>";
			
			if ($menu['NAMAFILE'])
				$ret .= anchor($menu['NAMAFILE'], $label);
			else
				$ret .= anchor('#', $label);

			if ($has_child)
				$ret .= "<ul>\n";

			$ret .= $this->_setMenu($menu['IDMENU']);

			if ($has_child)
				$ret .= "</ul>\n";

			$ret .= "</li>\n";

		}
		return $ret;
	}
	
	private function _menuHasChild($idmenu) {
        $ret = 0;
		foreach ($this->menu_unserialize as $menu) {

			if ($menu['IDMENUPARENT'] == $idmenu) {

				if ($menu['ISACTIVE'] && $this->menu_access[$menu['IDMENU']]) {
					$ret += $menu['IDMENU'];
                    return $ret;
				}

				$ret += $this->_menuHasChild($menu['IDMENU']);
			}
		}
        
		return $ret;
	}
	
	protected function checkRoleAuth($page=null) {
		if(!$page)
			$page = $this->router->class;
		
		// kombinasi hak akses
		$accessp = array();
		$accessp[$page] = $this->getAccess($page);
		
		if($this->method) {
			$page2 = "$page/{$this->method}";
			$accessp[$page2] = $this->getAccess($page2);
			
			if($this->accesscat) {
				$page2 .= $this->accesscat;
				$accessp[$page2] = $this->getAccess($page2);
			}
		}
        
		if($this->accesspage) {
			if(is_array($this->accesspage)) {
				$page2 = $this->accesspage[$this->method];
				if(empty($page2))
					$page2 = $this->accesspage['__'];
				if(!empty($page2))
					$accessp[$page2] = $this->getAccess($page2);
			}
			else {
				$page2 = $this->accesspage;
				$accessp[$page2] = $this->getAccess($page2);
			}
		}
        
		$access = 'CANREAD';
		if ($this->method == 'add')
			$access = 'CANINSERT';
		elseif ($this->method == 'edit')
			$access = 'CANUPDATE';
		elseif ($this->method == 'detail')
			$access = 'CANREAD';
		elseif ($this->method == 'delete')
			$access = 'CANDELETE';
		
		// cek kombinasi hak akses
		foreach($accessp as $page2 => $idmenu2) {
			if(empty($idmenu2))
				continue;
			if($this->menu_access[$idmenu2][$access]) {
				$idmenu = $idmenu2;
				break;
			}
		}
        
		if(empty($idmenu))
			$this->toNoData();
        
		$arr = array();
		if ($this->menu_access[$idmenu]['CANINSERT'])
			$arr[] = 'c_create';
		if ($this->menu_access[$idmenu]['CANREAD'])
			$arr[] = 'c_read';
		if ($this->menu_access[$idmenu]['CANUPDATE'])
			$arr[] = 'c_update';
		if ($this->menu_access[$idmenu]['CANDELETE'])
			$arr[] = 'c_delete';
		$this->auth = $arr;

		$this->data['c_create'] = $this->menu_access[$idmenu]['CANINSERT'];
		$this->data['c_read'] = $this->menu_access[$idmenu]['CANREAD'];
		$this->data['c_update'] = $this->menu_access[$idmenu]['CANUPDATE'];
		$this->data['c_delete'] = $this->menu_access[$idmenu]['CANDELETE'];
	}
	
	protected function getAccess($namafile) {
		$koderole = $_SESSION[G_SESSION]['koderole'];
       
		foreach ($this->akses as $k=>$rs) {
			foreach ($rs as $idunit=>$row) {
				if ($k == $koderole) {
					if (strtolower($row['NAMAFILE']) == $namafile) {
						return $row['IDMENU'];
					}
				}
			}
		}
		return false;
	}

	protected function isAuth($page, $access) {
		$koderole = $_SESSION[G_SESSION]['koderole'];
		foreach ($this->akses as $k=>$rs) {
			foreach ($rs as $idunit=>$row) {
				if ($k == $koderole) {
					if (strtolower($row['NAMAFILE']) == $page) {
						$idmenu = $row['IDMENU'];

						if ($this->menu_access[$idmenu][$access])
							return true;
						
						break;
					}
				}
			}
		}
		return false;
	}
	
	//protected function isAuth($page, $access) {
	//	$sql = "select idmenu from sc_menu where namafile='$page'";
	//	$idmenu = dbGetOne($sql);
	//	if (!$idmenu) 
	//		return false;
	//	if (!$this->menu_access[$idmenu][$access])
	//		return false;
	//	
	//	return true;
	//}
	
	protected function retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}

	protected function toNoData($message=null) {
		$template = 'default/';

		$this->data['message'] = $message;
		
		$this->view->layout = $template.'_layout';
		$this->view->data($this->data);
		$this->view->load(array( 
			'header'     => $template.'_header',    
			'menu'       => $template.'_menu',
			'notifikasi' => $template.'_notifikasi',  
			'footer'     => $template.'_footer',
			'content'    => $template.'error', 
		));
		$this->view->render();

		echo $this->output->get_output();
		exit;
	}

	protected function renderView($content, $only_content=false) {
		if ($this->layout_type == 'classic')
			$template = 'default/';
		else
			$template = 'mobile/';

		$this->view->data($this->data);

		$this->view_items = array();
		if ($only_content) {
			$this->view_items['content'] = $template.$content;
		}
		else {
			$this->view->layout = $template.'_layout';
			$this->view_items['header'] = $template .'_header';
			$this->view_items['menu'] = $template.'_menu';
			$this->view_items['notifikasi'] = $template.'_notifikasi';
			$this->view_items['content']  = $template.$content;
			$this->view_items['footer'] = $template.'_footer';
		}

		$this->view->load($this->view_items);
		$this->view->render();
	}

	protected function renderViewSimple($content) {
		if ($this->layout_type == 'classic')
			$template = 'default/';
		else
			$template = 'mobile/';

		$this->view->data($this->data);

		$this->view_items = array();
		$this->view->layout = $template.'_layout_simple';
		$this->view_items['content']  = $template.$content;
		$this->view_items['footer'] = $template.'_footer';
		
		$this->view->load($this->view_items);
		$this->view->render();
	}
	
	protected function logSession($act) {
        $this->load->database();
        
		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser']?$_SESSION[G_SESSION]['iduser']:array(null);
		$record['username'] = $_SESSION[G_SESSION]['username']?$_SESSION[G_SESSION]['username']:array(null);
		$record['keterangan'] = $act;
		$record['tipaddress'] = substr(xIPAddress(),0,75);; // substr($_SERVER['REMOTE_ADDR'],0,75);
		$record['useragent'] = substr($_SERVER['HTTP_USER_AGENT'],0,150);

		dbInsertTable('sessionlog', $record);
	}

	protected function curl($url, $fields) {
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
		
		if (!function_exists('curl_exec')) {
			# no curl
			$this->sendEmail($fields);
			
			return false;
		}
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_exec($ch);
		curl_close($ch);
	}

	protected function sendEmail($data) {
		if (!$this->config->item('allow_send_email'))
			return false;
		
		$this->load->library('mailer');
		
		$to = $data['to'];
		$cc = $data['cc'];
		$bcc = $data['bcc'];
		
		if($this->config->item('using_test_email'))
			$bcc = implode(',',array($bcc,$this->config->item('email_test')));

		$mail = $this->mailer->getMailer();

		$mail->IsSMTP();
		$mail->SMTPDebug = 0; // 2;
		$mail->SMTPAuth = $this->config->item('smtp_auth');
		if ($this->config->item('smtp_secure')) {
			$mail->SMTPSecure = $this->config->item('smtp_secure');
		}

		$mail->Host = $this->config->item('smtp_host');
		$mail->Port = $this->config->item('smtp_port'); 
		$mail->Username = $this->config->item('smtp_user');  
		$mail->Password = $this->config->item('smtp_pass');
		$mail->From = $this->config->item('email_from'); 
		$mail->FromName = $this->config->item('email_from_name'); 
		$mail->Subject = $data['subject'];
		
		if($data['ishtml'] !== false)
			$mail->IsHTML(true);
		
		$mail->Body = $data['body'];
		$to_list = explode(',', $to);
		foreach ($to_list as $to) {
			$mail->AddAddress($to);
		}
		
		if(!empty($cc)) {
			$cc_list = explode(',', $cc);
			foreach ($cc_list as $cc) {
				$mail->AddCC($cc);
			}
		}
		if(!empty($bcc)) {
			$bcc_list = explode(',', $bcc);
			foreach ($bcc_list as $bcc) {
				$mail->AddBCC($bcc);
			}
		}
        
		if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo; 
			$this->output->set_output($error);
            echo $error;
            die();
		}
		return true;
	}

	protected function createPassword($length){
	    $chrs = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z");

	    $num_chrs = count($chrs);
		$password = "";
	    for($i = 0; $i < $length; $i++){
	        $password .= $chrs[rand(0, $num_chrs - 1)];
	    }
	    return substr($password, 0, $length);
	}

	protected function renderPagination($site_url, $num_rows, $offset=0, $limit=10, $num_links=3) {
		if ($limit == -1 or $num_rows > $limit) {
			$this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = $site_url;
            $config['total_rows'] = $num_rows;
            $config['per_page'] = $limit;
			$config['num_links'] = $num_links;
			
			// cek url
			if(isset($this->uri_offset))
				$config['uri_segment'] = $this->uri_offset;
			
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
        }
	}

	protected function initFilter($filter) {
		$sql = '';
		
		if (!empty($filter)) {
			$groupfilter = explode(';',$filter);
			for ($h=0;$h<count($groupfilter);$h++)
			{
				$filterarray = explode(':',$groupfilter[$h]);
				for ($i=0;$i<count($filterarray);$i = $i + 3) 
				{
					$filterstr = '';
					$filtercol = $filterarray[$i];
					$filterdata = $filterarray[$i+1];
					if($filterdata == '')
						$filterdata = '*';
					$filtertype = $filterarray[$i+2];
					if($filtertype == '')
						$filtertype = 'C';
					
					// penggunaan alias
					$filtercol = $this->model->getAlias($filtercol);
					
					$filterop = '';
					// pemeriksaan operator perbandingan
					$arrop = array('<>','<=','>=','<','>','=');
					for ($n=0;$n<count($arrop);$n++) {
						$oppos = strpos($filterdata,$arrop[$n]);
						if ($oppos !== false) { // operator perbandingan ditemukan
							$filterop = $arrop[$n];
							$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
							break;
						}
					}
					if (!$filterop)
						$filterop = '='; // default operator
					
					switch ($filtertype) {
						case 'C' : 	// char atau varchar
									if (strstr($filtercol,'|')) { // sebelumnya ','
										$filtercols = explode('|', $filtercol);
										foreach ($filtercols as $k=>$filtercol) {
											if (empty($filtercol))
												continue;
											$filterstr .= 'lower('.$filtercol.") like '%".strtr(strtolower(trim($filterdata)),'*','%')."%' ";
											if ($k < count($filtercols)-1)
												$filterstr .= ' or ';
										}
									}
									else 
										$filterstr .= 'lower('.$filtercol.") like '%".strtr(strtolower(trim($filterdata)),'*','%')."%'";
									break;
						case 'I' : 	// integer
						case 'N' : 	// numeric atau float
									$filterstr .= $filtercol.' '.$filterop.' '.$filterdata;
									break;
						default	 :	// yang lain
									$filterstr .= $filtercol.' '.$filterop." '".$filterdata."'";
									break;
					}
					
					if(empty($sql))
						$sql .= ' where';
					else
						$sql .= ' and';
					$sql .= ' ('.$filterstr.')';
				}
			}
		}
		
		return $sql;
	}
	
	protected function initFilterTree($filter) {
		if(!empty($filter)) {
			$filterarr = array();
			foreach($filter as $key => $filterkey)
				$filterarr[] = $this->getFilterTreeCondition($key,$filterkey);
			
			return '('.implode(' and ',$filterarr).')';
		}
		else
			return '';
	}
	
	function getFilterTreeCondition($key,$filterkey) {
		$cfilterkey = array();
		foreach($filterkey as $filter) {
			$cfilter = $this->db->escape($filter);
			if(substr($filter,0,2) == '{{' and substr($filter,-2) == '}}')
				$cfilter = substr($cfilter,3,strlen($cfilter)-6);
			
			$cfilterkey[] = $cfilter;
		}
		
		return $this->model->getAlias($key).' in ('.implode(',',$cfilterkey).')';
	}
	
	protected function initFilterAll() {
		$filter = $_SESSION[G_SESSION][$this->ctl]['filter_all'];
		$filterlist = $_SESSION[G_SESSION][$this->ctl]['filter_all_field'];
		
		if(empty($filterlist))
			$a_list = $this->data['field_list'];
		else
			$a_list = array(array('name' => $filterlist));
		
		// membentuk filter
		$a_filter = array();
		foreach($a_list as $list) {
			if($list['filter'] === false or empty($list['name']))
				continue;
			
			$col = $this->model->getAlias($list['name']);
			
			$a_filter[] = "lower($col) like '%".strtolower($filter)."%'";
		}
		
		if(!empty($filter)) {
			$filter = $this->model->getFilter();
			
			if(empty($filter))
				$keyword = ' where ';
			else
				$keyword = ' and ';
			
			$this->model->addFilter($keyword.'('.implode(' or ',$a_filter).')');
		}
	}

	protected function responsePost() {
		$act = $this->input->post('act');
		if ($act === 'add_mode' && $this->data['c_create']) //  && !$this->input->post('key'))
			redirect ("/$this->ctl/add/".$this->input->post('key'));
		elseif ($act === 'detail_mode' && $this->data['c_read'] && $this->input->post('key'))
			redirect ("/$this->ctl/detail/".$this->input->post('key'));
		elseif ($act === 'edit_mode' && $this->data['c_update'] && $this->input->post('key'))
			redirect ("/$this->ctl/edit/".$this->input->post('key'));
		elseif ($act === 'delete_mode' && $this->data['c_delete'] && $this->input->post('key'))
			redirect ("/$this->ctl/delete/".$this->input->post('key'));
		elseif ($act === 'list') {
			$offset = (int) $_SESSION[G_SESSION][$this->ctl]['offset'];
			$this->backToList($offset);
		}
		elseif ($act === 'do_filter') {
			// unset($_SESSION[G_SESSION][$this->ctl]);
			unset($_SESSION[G_SESSION][$this->ctl]['offset']);
			unset($_SESSION[G_SESSION][$this->ctl]['filter_sort']);
			unset($_SESSION[G_SESSION][$this->ctl]['filter_num_record']);
			
			$filter = $_SESSION[G_SESSION][$this->ctl]['filter_string'];
			if(empty($filter))
				$filter = '';
			else
				$filter .= ';';
			$filter .= $this->input->post('filter_string');
			
			$_SESSION[G_SESSION][$this->ctl]['filter_string'] = $filter;
			
			redirect ("$this->lstctl"); // /".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'do_sort') {
			$filter_sort = $this->input->post('filter_sort');
			$sort = 'asc';
			if (stristr($filter_sort,'desc'))
				$sort = 'desc';
				
			/* if (strstr($filter_sort,',')) {
				$columns = explode(',', $filter_sort);
				$filter_sort = '';
				foreach ($columns as $column) {
					if (!preg_match('(asc|desc)', $column))
						$filter_sort .= $column . ' ' . $sort . ', ';
					else
						$filter_sort .= $column;
				}
			} */
			
			$columns = explode(',', $filter_sort);
			$filter_sort = array();
			foreach ($columns as $column) {
				$column = trim($column);
				$pos = strpos($column,' ');
				
				if($pos !== false) {
					$sort = trim(substr($column,$pos+1));
					$column = substr($column,0,$pos);
				}
				
				$column = $this->model->getAlias($column);
				$filter_sort[] = $column . ' ' . $sort;
			}
			$filter_sort = implode(', ',$filter_sort);
			
			$_SESSION[G_SESSION][$this->ctl]['filter_sort'] = $filter_sort;
			redirect ("$this->lstctl/".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'num_record') {
			unset($_SESSION[G_SESSION][$this->ctl]['offset']);
			$_SESSION[G_SESSION][$this->ctl]['filter_num_record'] = $this->input->post('filter_num_record');
			redirect ("$this->lstctl/".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'reset_filter') {
			unset($_SESSION[G_SESSION][$this->ctl]);
			redirect ("$this->lstctl");
		}
		elseif ($act === 'do_filter_tree') {
			list($key,$val) = explode(':',$this->input->post('filter_string'));
			
			if(isset($_SESSION[G_SESSION][$this->ctl]['filter_tree'][$key][$val])) {
				unset($_SESSION[G_SESSION][$this->ctl]['filter_tree'][$key][$val]);
				if(empty($_SESSION[G_SESSION][$this->ctl]['filter_tree'][$key]))
					unset($_SESSION[G_SESSION][$this->ctl]['filter_tree'][$key]);
				if(empty($_SESSION[G_SESSION][$this->ctl]['filter_tree']))
					unset($_SESSION[G_SESSION][$this->ctl]['filter_tree_string']);
			}
			else {
				$_SESSION[G_SESSION][$this->ctl]['filter_tree'][$key][$val] = $val;
				$_SESSION[G_SESSION][$this->ctl]['filter_tree_string'] = $this->input->post('filter_string');
			}
			
			redirect ("$this->lstctl");
		}
		elseif ($act == 'do_filter_all') {
			$_SESSION[G_SESSION][$this->ctl]['filter_all'] = $this->input->post('filter_all');
			
			$filterlist = $this->input->post('filter_list');
			if(isset($filterlist))
				$_SESSION[G_SESSION][$this->lstctl]['filter_all_field'] = $filterlist;
			
			redirect ("$this->lstctl");
		}
		elseif (substr($act,0,13) === 'remove_filter') {
			$idx = substr($act,14);
			
			$gfilter = explode(';',$_SESSION[G_SESSION][$this->ctl]['filter_string']);
			unset($gfilter[$idx]);
			
			$_SESSION[G_SESSION][$this->ctl]['filter_string'] = implode(';',$gfilter);
			
			redirect ("$this->lstctl"); // /".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
	}
	
	protected function setModelFilter() {
		$this->model->setFilter($this->initFilter($_SESSION[G_SESSION][$this->ctl]['filter_string']));
		$this->model->setSort($_SESSION[G_SESSION][$this->ctl]['filter_sort']);
		
		$filter_tree = $this->initFilterTree($_SESSION[G_SESSION][$this->ctl]['filter_tree']);
		if(!empty($filter_tree)) {
			$filter = $this->model->getFilter();
			
			if(empty($filter))
				$keyword = ' where ';
			else
				$keyword = ' and ';
			
			$this->model->addFilter($keyword.$filter_tree);
		}
		
		$this->data['filter_string'] = $_SESSION[G_SESSION][$this->ctl]['filter_string'];
	}
	
	##########################
	
	private function _setStickyNews() {
		$sql = "select val from misc where idmisc='sticky_news' and ispublish=1 ";
		$this->data['sticky_news'] = dbGetOne($sql);		
	}

	private function initLayoutType() {
		if(!class_exists('Mobile_Detect')){ return 'classic'; }
	
		$detect = new Mobile_Detect;
		$isMobile = $detect->isMobile();
		$isTablet = $detect->isTablet();
	
		$layoutTypes = array('classic', 'mobile', 'tablet');
	
		if ( isset($_GET['layoutType']) ){
			$layoutType = $_GET['layoutType'];
		} else {
			if(empty($_SESSION[G_SESSION]['layoutType'])){
				$layoutType = ($isMobile ? ($isTablet ? 'tablet' : 'mobile') : 'classic');	
			} else {
				$layoutType =  $_SESSION[G_SESSION]['layoutType'];
			}
		}
	
		if( !in_array($layoutType, $layoutTypes) ){ $layoutType = 'classic'; }
	
		$this->layout_type = $_SESSION[G_SESSION]['layoutType'] = $layoutType;
	
		return $layoutType;
	}

	protected function ePengaduanBaru($idpengaduan) {
		$sql = "select * from pengaduan where idpengaduan=$idpengaduan";
		$post = dbGetRow($sql);

		$isi = $post['isi'];
		$subyek = $post['subyek'];
		$nomor_pengaduan = $post['nomor'];
		
		$sql = "select u.nama, p.nama as namapeg from sc_user u left join v_pegawai p on u.username=p.nippeg where iduser={$post['iduser']}";
		$user = dbGetRow($sql);

		$sql = "select * from emailtemplate where idemailtemplate='pengaduan_baru'";
		$template = dbGetRow($sql);
		
		$url = site_url('publ1c/sendemail');

		$sql = "select u.email, u.nama from sc_user u join pengaduanunitstaff s on u.iduser=s.idstaff
		where idpengaduanunit={$post['idpengaduanunit']}";
		$staffs = dbGetRows($sql);
		foreach ($staffs as $staff) {
			$nama_penerima = $staff['nama'];
			$to = $staff['email'];
			
			$email_subject = $template['subject'];
			$email_body = $template['body'];
			
			# subject
			if (preg_match_all("/{{(.*?)}}/", $email_subject, $m)) {
				foreach ($m[1] as $i => $varname) {
					$email_subject = str_replace($m[0][$i], sprintf('%s', $$varname), $email_subject);
				}
			}		
	
			# body
			if (preg_match_all("/{{(.*?)}}/", $email_body, $m)) {
				foreach ($m[1] as $i => $varname) {
					$email_body = str_replace($m[0][$i], sprintf('%s', $$varname), $email_body);
				}
			}
			
			$fields = array(
							'to'=>$to,
							'subject'=>$email_subject,
							'body'=>nl2br($email_body),
							'publ1c_pass'=>$this->config->item('encryption_key')
							);
			$this->curl($url,$fields);		
		}		
	}
	
	protected function _updateSessionData() {
		$sess_name = session_id();
		$sql = "select 1 from sessiondata where sessname='$sess_name'";
		if (dbGetOne($sql)) {
			$sql = "update sessiondata set tupdatetime=current_timestamp where sessname='$sess_name'";
			dbQuery($sql);
		}
		else {
			$record = array();
			$record['username'] = $_SESSION[G_SESSION]['username'];
			$record['sessname'] = $sess_name;
			$record['tipaddress'] = xIPAddress(); // $_SERVER['REMOTE_ADDR'];
			$record['tupdatetime'] = array('current_timestamp');
			dbInsertTable('sessiondata', $record);			
		}
		
		$sql = "delete from sessiondata where hour(timediff(current_timestamp, tupdatetime))>=1";
		dbQuery($sql);
	}
	function CekRoleUserLogin(){
	
	$role=$_SESSION[G_SESSION]['koderole'];
	
	if ($role=='M')
		$islogin='mahasiswa';
	
	else if ($role=='S')
		$islogin='dosen';
	
	else if ($role=='A')
		$islogin='administrator';

		return $islogin;
	}
	
	// bagian sim akademik (bisa dihapus bila disalin ke sim lain)
	
	// cek mengajar
	function cekMengajar($kodekelas) {
		if(xIsDosen()) {
			$this->load->model('mengajar_model','Mengajar');
			
			if(!$this->Mengajar->isDosenMengajar(xUserName(),$kodekelas))
				$this->toNoData('Anda tidak berhak mengakses data ini');
		}
	}
	function ReadOnly($id){
		if (!$id){ 
		$this->data['edit']=true;
		}
	}

	protected function validateSession($force=false) {
		$session_id = $_SESSION[G_SESSION]['sessionid'];

		$session_id = $this->db->escape($session_id);
		$gate_schema = $this->config->item('schema_gate');
        $sql = "select 1 from {$gate_schema}.sc_usersession where sessionid=$session_id";
		$result = dbGetOne($sql);
		
		if (!$result) {
			unset($_SESSION[G_SESSION]);
			redirect($this->config->item('gate_login_url'));
		}
	}

	protected function validateSession2($force=false) {
		if (!$force) {
			if (!$_SESSION[G_SESSION]['last_update_session'])
				$_SESSION[G_SESSION]['last_update_session'] = time();
	
			if (time() - $_SESSION[G_SESSION]['last_update_session'] < $this->config->item('sess_db_update_time'))
				return false;
		}

		$key = $this->config->item('gate_key');
		$gate_url = $this->config->item('gate_session_url') . '/validate';

		$session_id = xEncrypt($_SESSION[G_SESSION]['sessionid'], $key);

		$fields = array(
						's' => rawurlencode($session_id),
						);

		foreach($fields as $k=>$v) { $fields_string .= $k.'='.$v.'&'; }
		rtrim($fields_string, '&');
		
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, $gate_url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		
		$result = curl_exec($ch);
		curl_close($ch);

		if (!$result) {
			unset($_SESSION[G_SESSION]);
			redirect($this->config->item('gate_login_url'));
		}
	}

}
