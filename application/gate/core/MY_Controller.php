<?php
require_once( COREPATH . 'core/Base_Controller.php' );

class MY_Controller extends Base_Controller 
{
    protected $layout;
    
    function __construct()
    {
        parent::__construct();
        
        $this->lang->load('general', 'id');
    }
    
}
?>
