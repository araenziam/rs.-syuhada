<table id="table" class="gtable sortable">
    <thead>
        <tr>
            <?php 
            if ( isset($kolom) ) {
            foreach ( $kolom as $col ) {?>
                <th><?php echo $col['label']?></th>
            <?php } }?>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
            <?php foreach( $arr_data['data'] as $row) {
                $s_key = get_key_value($key, $row, $separator);
            ?>
            <tr>
                <?php foreach ( $kolom as $col ) {?>
                <td><?php echo $row[$col['kolom']]?></td>                
                <?php }?>
                <td width="10%">
                    <a title="Edit" href="<?= $uriclass. '/edit/'.$s_key?>"><div class="btn-edit"></div></a>
                    <a onclick="return confirm('Apakah yakin hapus data ini?');" title="Delete" href="<?= $uriclass. '/delete/'.$s_key?>"><div class="btn-delete"></div></a>
                </td>
            </tr>
        <?php }?>
    </tbody>
</table>
<input type="hidden" name="act">
<input type="hidden" name="key">